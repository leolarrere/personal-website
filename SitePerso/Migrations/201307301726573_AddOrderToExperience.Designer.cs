// <auto-generated />
namespace SitePerso.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddOrderToExperience : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddOrderToExperience));
        
        string IMigrationMetadata.Id
        {
            get { return "201307301726573_AddOrderToExperience"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
