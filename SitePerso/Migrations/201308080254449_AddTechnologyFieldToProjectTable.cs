namespace SitePerso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTechnologyFieldToProjectTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "technology", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "technology");
        }
    }
}
