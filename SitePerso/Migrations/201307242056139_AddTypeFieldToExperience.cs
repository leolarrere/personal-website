namespace SitePerso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTypeFieldToExperience : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Experiences", "type", c => c.String());
            AlterColumn("dbo.Experiences", "startYear", c => c.Int());
            AlterColumn("dbo.Experiences", "endYear", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Experiences", "endYear", c => c.Int(nullable: false));
            AlterColumn("dbo.Experiences", "startYear", c => c.Int(nullable: false));
            DropColumn("dbo.Experiences", "type");
        }
    }
}
