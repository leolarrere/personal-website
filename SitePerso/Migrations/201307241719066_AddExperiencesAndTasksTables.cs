namespace SitePerso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExperiencesAndTasksTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Experiences",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        startMonth = c.String(),
                        endMonth = c.String(),
                        startYear = c.Int(nullable: false),
                        endYear = c.Int(nullable: false),
                        imgURL = c.String(),
                        companyName = c.String(),
                        jobDescription = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        title = c.String(),
                        description = c.String(),
                        idExperience = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Experiences", t => t.idExperience, cascadeDelete: true)
                .Index(t => t.idExperience);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tasks", new[] { "idExperience" });
            DropForeignKey("dbo.Tasks", "idExperience", "dbo.Experiences");
            DropTable("dbo.Tasks");
            DropTable("dbo.Experiences");
        }
    }
}
