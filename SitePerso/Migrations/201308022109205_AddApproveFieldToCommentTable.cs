namespace SitePerso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddApproveFieldToCommentTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "approved", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comments", "approved");
        }
    }
}
