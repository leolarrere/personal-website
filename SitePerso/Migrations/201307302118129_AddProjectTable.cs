namespace SitePerso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProjectTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        date = c.String(),
                        imgURL = c.String(),
                        institution = c.String(),
                        shortDescription = c.String(),
                        description = c.String(),
                        order = c.Int(),
                    })
                .PrimaryKey(t => t.id);
            
            DropColumn("dbo.Experiences", "type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Experiences", "type", c => c.String());
            DropTable("dbo.Projects");
        }
    }
}
