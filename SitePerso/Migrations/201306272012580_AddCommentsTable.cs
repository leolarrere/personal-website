namespace SitePerso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCommentsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        username = c.String(maxLength: 50),
                        content = c.String(maxLength: 1000),
                        idPost = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Posts", t => t.idPost, cascadeDelete: true)
                .Index(t => t.idPost);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "idPost" });
            DropForeignKey("dbo.Comments", "idPost", "dbo.Posts");
            DropTable("dbo.Comments");
        }
    }
}
