namespace SitePerso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderToExperience : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Experiences", "order", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Experiences", "order");
        }
    }
}
