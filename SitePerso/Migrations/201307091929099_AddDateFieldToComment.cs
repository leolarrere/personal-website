namespace SitePerso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateFieldToComment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comments", "date");
        }
    }
}
