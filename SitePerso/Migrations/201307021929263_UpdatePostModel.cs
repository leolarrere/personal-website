namespace SitePerso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePostModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "date", c => c.DateTime(nullable: false));
            AddColumn("dbo.Posts", "fileName", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "fileName");
            DropColumn("dbo.Posts", "date");
        }
    }
}
