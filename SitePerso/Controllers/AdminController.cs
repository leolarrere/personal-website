﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SitePerso.Models;

namespace SitePerso.Controllers
{
    public class AdminController : Controller
    {
        public ViewResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(Credentials model)
        {
            if (ModelState.IsValid)
            {
                if (FormsAuthentication.Authenticate(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    return Redirect(Url.Action("Index", "Admin") ?? Url.Action("LogOn", "Admin"));  
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect username/password combination");
                }
            }

            return View();
        }

        [Authorize]
        public ViewResult Index()
        {
            return View();
        }
    }
}
