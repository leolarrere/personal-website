﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SitePerso.Models;
using System.Diagnostics;

namespace SitePerso.Controllers
{
    public class BlogController : Controller
    {
        private DB db = new DB();

        public ActionResult Index()
        {
            return View(db.Posts.OrderByDescending(p => p.date));
        }

        public ActionResult Post(int id = 0, Boolean alert = false)
        {            
            Post post = db.Posts.Find(id);
            
            if (post == null)
            {
                return HttpNotFound();
            }

            if (User.Identity.IsAuthenticated)
                ViewBag.comments = post.Comments;
            else
                ViewBag.comments = post.Comments.Where(p => p.approved == true);

            if (alert)
                ViewBag.alert = "<div class='row-fluid'><div class='alert alert-success alert-blog text-center'>Thanks! Your comment will be visible as soon as I approve it.</div></div>";

            return View(post);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(Post post, HttpPostedFileBase file)
        {
            if (file != null)
            {
                var fileName = System.IO.Path.GetFileName(file.FileName);
                var path = System.IO.Path.Combine(Server.MapPath("~/Content/images/blog"), fileName);
                file.SaveAs(path);
                post.fileName = fileName;
            }

            post.date = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post);
        }

        [Authorize]
        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                Post dbPost = db.Posts.Find(post.id);
                dbPost.title = post.title;
                dbPost.content = post.content;
                //dbPost.date = DateTime.Now;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        [HttpPost]
        public ActionResult addComment(int idPost, string username, string comment)
        {
            Post post = db.Posts.Find(idPost);
            Comment c = new Comment();
            c.idPost = idPost;
            c.post = db.Posts.Find(idPost);
            c.username = username;
            c.content = comment;
            c.date = DateTime.Now;
            c.approved = false;
            db.Comments.Add(c);
            db.SaveChanges();

            return RedirectToAction("Post", new { id = post.id, alert = true });
        }

        [Authorize]
        public ActionResult Approve(int id = 0, Boolean value = false)
        {
            Comment c = db.Comments.Find(id);
            c.approved = value;
            db.SaveChanges();

            return RedirectToAction("Post", new { id = c.idPost });
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}