﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;

using SitePerso.Models;

namespace SitePerso.Controllers
{
    public class HomeController : Controller
    {
        private DB db = new DB();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Education()
        {
            return View();
        }

        public ActionResult Trivia()
        {
            return View();
        }

        public ActionResult Experience()
        {            
            return View(db.Experiences.OrderBy(e => e.order));
        }

        public ActionResult Skills()
        {
            return View(db.Projects.OrderBy(e => e.order));
        }

        [HttpPost]
        public JsonResult SendMail(Contact c)
        {
            if (ModelState.IsValid)
            {                
                MailMessage m = new MailMessage();
                SmtpClient sc = new SmtpClient();

                m.IsBodyHtml = true;

                m.From = new MailAddress("admin@leolarrere.com", "Leolarrere.com");
                m.To.Add(new MailAddress("llarrere@iit.edu", "Leo Larrere"));
                m.Subject = "You've got a message";
                m.Body = "Name : " + c.name + "<br/>";
                m.Body += "Email : " + c.email + "<br/>";
                m.Body += "Phone : " + c.phone + "<br/><br/>";
                m.Body += "Message : " + c.message;

                try
                {
                    sc.Send(m);
                }
                catch (Exception e)
                {
                    return Json(new { success = false, msg = e.ToString() });
                
                }
                return Json(new { success = true, msg = "success" });
            }
            else
            {
                var errors = ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList();
                string errorMsg = string.Join("<br/>", errors);
                return Json(new { success = false, msg = errorMsg });
            }
        }
    }
}
