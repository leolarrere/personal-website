﻿$('.drag.constrain-to-window .drag-item').pep(
    {
    useCSSTranslation: false,
    constrainTo: 'window'
    }
)

$(document).ready(function ()
{
    animateLogos();

    setTimeout(function ()
    {
        var offset = 110;
        if ($(".content").width() < 865) { offset = 90 }
        $('#tooltip').tooltip('show');
        moveTooltip(offset);
    }, 1000);

    $('.drag-item').click(function () { $('#tooltip').tooltip('destroy'); });
});

function moveTooltip(offset)
{
    $('.tooltip').css("left", $("#asp").offset().left - offset);
    $('.tooltip').css("top", $("#asp").offset().top + 0.018 * $("#container").width());
}

function animateLogos()
{
    $('#html5').fadeIn(1000, function () { $('#html5').animate({ "left": $(".content").width() * 0.1, "top": $(".drag").height() * 0.1 + 380 }, 1000); });
    $('#asp').fadeIn(1000, function () { $('#asp').animate({ "left": $(".content").width() * 0.47, "top": $(".drag").height() * 0.3 + 380 }, 1000); });
    $('#css3').fadeIn(1000, function () { $('#css3').animate({ "left": $(".content").width() * 0.65, "top": $(".drag").height() * 0.33 + 380 }, 1000); });
    $('#js').fadeIn(1000, function () { $('#js').animate({ "left": $(".content").width() * 0.8, "top": 380 }, 1000); });
    $('#php').fadeIn(1000, function () { $('#php').animate({ "left": $(".content").width() * 0.1, "top": $(".drag").height() * 0.6 + 380 }, 1000); });
    $('#sqlserver').fadeIn(1000, function () { $('#sqlserver').animate({ "left": $(".content").width() * 0.32, "top": $(".drag").height() * 0.83 + 380 }, 1000); });
    $('#java').fadeIn(1000, function () { $('#java').animate({ "left": $(".content").width() * 0.23, "top": $(".drag").height() * 0.035 + 380 }, 1000); });
    $('#mysql').fadeIn(1000, function () { $('#mysql').animate({ "left": $(".content").width() * 0.33, "top": $(".drag").height() * 0.48 + 380 }, 1000); });
    $('#ror').fadeIn(1000, function () { $('#ror').animate({ "left": $(".content").width() * 0.8, "top": $(".drag").height() * 0.58 + 380 }, 1000); });
    $('#ajax').fadeIn(1000, function () { $('#ajax').animate({ "left": $(".content").width() * 0.66, "top": $(".drag").height() * 0.04 + 380 }, 1000); });
    $('#bootstrap').fadeIn(1000, function () { $('#bootstrap').animate({ "left": $(".content").width() * 0.45, "top": $(".drag").height() * 0.05 + 380 }, 1000); });
    $('#jquery').fadeIn(1000, function () { $('#jquery').animate({ "left": $(".content").width() * 0.49, "top": $(".drag").height() * 0.77 + 380 }, 1000); });
}

function moveLogos()
{
    $('#html5').css("left", $(".content").width() * 0.1);
    $('#asp').css("left", $(".content").width() * 0.47);
    $('#css3').css("left", $(".content").width() * 0.65);
    $('#js').css("left", $(".content").width() * 0.8);
    $('#php').css("left", $(".content").width() * 0.1);
    $('#sqlserver').css("left", $(".content").width() * 0.32);
    $('#java').css("left", $(".content").width() * 0.23);
    $('#mysql').css("left", $(".content").width() * 0.33);
    $('#ror').css("left", $(".content").width() * 0.8);
    $('#ajax').css("left", $(".content").width() * 0.66);
    $('#bootstrap').css("left", $(".content").width() * 0.45);
    $('#jquery').css("left", $(".content").width() * 0.49);

    $('#html5').css("top", $(".drag").height() * 0.1 + 380);
    $('#asp').css("top", $(".drag").height() * 0.3 + 380);
    $('#css3').css("top", $(".drag").height() * 0.33 + 380);
    $('#js').css("top", 380);
    $('#php').css("top", $(".drag").height() * 0.6 + 380);
    $('#sqlserver').css("top", $(".drag").height() * 0.83 + 380);
    $('#java').css("top", $(".drag").height() * 0.035 + 380);
    $('#mysql').css("top", $(".drag").height() * 0.48 + 380);
    $('#ror').css("top", $(".drag").height() * 0.58 + 380);
    $('#ajax').css("top", $(".drag").height() * 0.04 + 380);
    $('#bootstrap').css("top", $(".drag").height() * 0.05 + 380);
    $('#jquery').css("top", $(".drag").height() * 0.77 + 380);

    resizeLogos();
}

function resizeLogos()
{
    $('#html5').css("width", $(".content").width() * 0.12);
    $('#asp').css("width", $(".content").width() * 0.12);
    $('#css3').css("width", $(".content").width() * 0.12);
    $('#js').css("width", $(".content").width() * 0.12);
    $('#php').css("width", $(".content").width() * 0.18);
    $('#sqlserver').css("width", $(".content").width() * 0.13);
    $('#java').css("width", $(".content").width() * 0.21);
    $('#mysql').css("width", $(".content").width() * 0.09);
    $('#ror').css("width", $(".content").width() * 0.08);
    $('#ajax').css("width", $(".content").width() * 0.12);
    $('#bootstrap').css("width", $(".content").width() * 0.17);
    $('#jquery').css("width", $(".content").width() * 0.19);
}
