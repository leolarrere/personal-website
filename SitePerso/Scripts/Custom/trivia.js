﻿$(document).ready(function ()
{
    $('#popoverRoadTrips').popover({ html: true });
    $('#tooltip').tooltip('show');

    moveTooltip();
    FlightsMap();
    WorldMap();

    /*
    $("#popoverWorld").click(function () {
        $('#tooltip-map').tooltip('destroy');
        $('#popoverStates').popover('hide');
        $(".popover").width(632);
        setTimeout("movePopover('world')", 10);
        setTimeout("createWorldMap()", 50);
    });

    $("#popoverStates").click(function () {
        $('#popoverWorld').popover('hide');
        $(".popover").width(528);
        setTimeout("movePopover('states')", 10);
        setTimeout("createStatesMap()", 50);
    });
    */
});

function moveTooltip()
{
    $('.tooltip').css("left", $("#popoverRoadTrips").offset().left - 72);
    $('.tooltip').css("top", $("#popoverRoadTrips").offset().top);
}

function movePopover(popover)
{
    //
    $(".popover").css('top', $("#popoverRoadTrips").offset().top - $("#roadTrips").height() - 22);
    $(".popover").css('left', $("#popoverRoadTrips").offset().left - $("#roadTrips").width() / 2 - 6);
}