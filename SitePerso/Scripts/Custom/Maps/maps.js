﻿function WorldMap() {
    $('#WorldMap').vectorMap(
    {
        map: 'world_mill_en',
        backgroundColor: 'none',
        regionStyle:
        {
            initial:
            {
                fill: 'gainsboro',
                "fill-opacity": 1,
                stroke: 'none',
                "stroke-width": 1,
                "stroke-opacity": 1
            },
            hover:
            {
                "fill-opacity": 0.8
            },
            selected:
            {
                fill: 'yellow'
            },
            selectedHover:
            {
            }
        },
        series: {
            regions: [{
                values: {
                    FR: '#7db9e8',
                    DE: '#7db9e8',
                    ES: '#7db9e8',
                    AO: '#7db9e8',
                    CM: '#7db9e8',
                    MG: '#7db9e8',
                    BS: '#7db9e8',
                    US: '#7db9e8',
                    NA: '#7db9e8',
                    GB: '#7db9e8',
                    IT: '#7db9e8',
                    CZ: '#7db9e8',
                    CU: '#7db9e8',
                    BE: '#7db9e8',
                    LU: '#7db9e8',
                    NL: '#7db9e8',
                    DK: '#7db9e8',
                    ZW: '#7db9e8',
                }
            }]
        },
        markerStyle: {
            initial: {
                fill: 'whitesmoke',
                stroke: '#333333',
                "stroke-width": 2,
                "stroke-opacity": 1,
                r: 5
            }
        },
        markers: [{
            latLng: [55.7517, 37.6178],
            name: 'Moscow'
        }, {
            latLng: [59.9333, 30.3333],
            name: 'Saint Petersburg'
        }, {
            latLng: [41.0128, 28.9744],
            name: 'Istanbul'
        }, ],
    });
}

function StatesMap() {
    $('#mapStates').vectorMap(
    {
        map: 'us_aea_en',
        backgroundColor: 'white',
        regionStyle:
        {
            initial:
            {
                fill: 'gainsboro',
                "fill-opacity": 1,
                stroke: 'none',
                "stroke-width": 1,
                "stroke-opacity": 1
            },
            hover:
            {
                "fill-opacity": 0.8
            },
            selected:
            {
                fill: 'yellow'
            },
            selectedHover:
            {
            }
        },
        series: {
            regions: [{
                values: {
                    "US-AZ": 'skyblue',
                    "US-CA": 'skyblue',
                    "US-CO": 'skyblue',
                    "US-FL": 'skyblue',
                    "US-GA": 'skyblue',
                    "US-IL": 'skyblue',
                    "US-IN": 'skyblue',
                    "US-MI": 'skyblue',
                    "US-MN": 'skyblue',
                    "US-NC": 'skyblue',
                    "US-NV": 'skyblue',
                    "US-NY": 'skyblue',
                    "US-SC": 'skyblue',
                    "US-UT": 'skyblue',
                    "US-WA": 'skyblue',
                }
            }]
        },
        markerStyle: {
            initial: {
                fill: 'whitesmoke',
                stroke: '#333333',
                "stroke-width": 2,
                "stroke-opacity": 1,
                r: 5
            }
        }
    });
}

function FlightsMap() {
    var map;

    // svg path for target icon
    var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
    // svg path for plane icon
    var planeSVG = "M19.671,8.11l-2.777,2.777l-3.837-0.861c0.362-0.505,0.916-1.683,0.464-2.135c-0.518-0.517-1.979,0.278-2.305,0.604l-0.913,0.913L7.614,8.804l-2.021,2.021l2.232,1.061l-0.082,0.082l1.701,1.701l0.688-0.687l3.164,1.504L9.571,18.21H6.413l-1.137,1.138l3.6,0.948l1.83,1.83l0.947,3.598l1.137-1.137V21.43l3.725-3.725l1.504,3.164l-0.687,0.687l1.702,1.701l0.081-0.081l1.062,2.231l2.02-2.02l-0.604-2.689l0.912-0.912c0.326-0.326,1.121-1.789,0.604-2.306c-0.452-0.452-1.63,0.101-2.135,0.464l-0.861-3.838l2.777-2.777c0.947-0.947,3.599-4.862,2.62-5.839C24.533,4.512,20.618,7.163,19.671,8.11z";

    AmCharts.ready(function () {
        map = new AmCharts.AmMap();
        map.pathToImages = "http://www.ammap.com/lib/images/";

        var dataProvider = {
            mapVar: AmCharts.maps.worldLow
        };

        map.areasSettings = {
            unlistedAreasColor: "gainsboro"
        };

        map.imagesSettings = {
            color: "#CC0000",
            rollOverColor: "#CC0000",
            selectedColor: "#000000"
        };

        map.linesSettings = {
            color: "#CC0000",
            alpha: 0.4
        };

        map.z

        //Paris
        var paris =
        {
            id: "Paris",
            color: "#000000",
            svgPath: targetSVG,
            title: "Paris",
            latitude: 48.8742,
            longitude: 2.3470,
            scale: 1.5,
            zoomLevel: 3.4,
            zoomLongitude: -42,
            zoomLatitude: 30,

            lines:
            [
                {
                    //Paris - New York
                    latitudes: [48.8742, 40.4300],
                    longitudes: [2.3470, -74.0000]
                },
                {
                    //Paris - Atlanta
                    latitudes: [48.8742, 33.7489],
                    longitudes: [2.3470, -84.3881]
                },
                {
                    //Paris - FL
                    latitudes: [48.8742, 26.1219],
                    longitudes: [2.3470, -80.1436]
                },
                {
                    //Paris - Pau
                    latitudes: [48.8742, 43.3017],
                    longitudes: [2.3470, -0.1]
                },
                {
                    //Paris - Chicago
                    latitudes: [48.8742, 41.850],
                    longitudes: [2.3470, -87.6500]
                },
                {
                    //Paris - Douala
                    latitudes: [48.8742, 4.0500],
                    longitudes: [2.3470, 9.7000]
                },
                {
                    //Paris - Bordeaux
                    latitudes: [48.8742, 44.8386],
                    longitudes: [2.3470, -0.5783]
                }
            ],

            images: [{
                label: "Flights from Paris",
                svgPath: planeSVG,
                left: 90,
                top: 20,
                labelShiftY: 5,
                color: "#CC0000",
                labelColor: "#CC0000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 20
            },
            {
                label: "Show flights within the US",
                left: 96,
                top: 45,
                labelColor: "#000000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 11,
                linkToObject: "US"
            },
            {
                label: "Show all flights",
                left: 96,
                top: 60,
                labelColor: "#000000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 11,
                linkToObject: "All"
            }]
        };

        //US flights
        var US =
        {
            id: "US",
            color: "#000000",
            svgPath: targetSVG,
            title: "Miami / Ft Lauderdale / West Palm Beach",
            latitude: 26.1219,
            longitude: -80.1436,
            scale: 1.5,
            zoomLevel: 6,
            zoomLongitude: -100,
            zoomLatitude: 41,

            lines:
            [
                {   //FL - Chicago
                    latitudes: [26.1219, 41.8500],
                    longitudes: [-80.1436, -87.8500]
                },
                {   //FL - San Francisco
                    latitudes: [26.1219, 37.7750],
                    longitudes: [-80.1436, -122.4183]
                },
                {
                    //FL - Atlanta
                    latitudes: [26.1219, 33.7489],
                    longitudes: [-80.1436, -84.3881]
                },
                {
                    //FL - Phoenix
                    latitudes: [26.1219, 33.4482],
                    longitudes: [-80.1436, -112.0738]
                },
                {
                    //FL - Las Vegas
                    latitudes: [26.1219, 36.0800],
                    longitudes: [-80.1436, -115.1522]
                },
                {
                    //Las Vegas - Dallas
                    latitudes: [36.0800, 32.7828],
                    longitudes: [-115.1522, -96.8039]
                },
                {
                    //Dallas - FL
                    latitudes: [32.7828, 26.1219],
                    longitudes: [-96.8039, -80.1436]
                },
                {
                    //FL - New York
                    latitudes: [26.1219, 40.43],
                    longitudes: [-80.1436, -74]
                },
                {
                    //FL - Washington
                    latitudes: [26.1219, 38.8900],
                    longitudes: [-80.1436, -77.0300]
                },
                {
                    //Washington - New York
                    latitudes: [38.8900, 40.43],
                    longitudes: [-77.0300, -74]
                },
                {
                    //Chicago - Seattle
                    latitudes: [41.8500, 47.6097],
                    longitudes: [-87.6500, -122.3331]
                },
                {
                    //New York - Chicago
                    latitudes: [40.43, 41.8500],
                    longitudes: [-74, -87.6500]
                },
                {
                    //Las Vegas - Cincinnati
                    latitudes: [36.0800, 39.1619],
                    longitudes: [-115.1522, -84.4569]
                },
                {
                    //Cincinnati - FL
                    latitudes: [39.1619, 26.1219],
                    longitudes: [-84.4569, -80.1436]
                },
                {
                    //Denver - FL
                    latitudes: [39.7392, 26.1219],
                    longitudes: [-104.9842, -80.1436]
                }
            ],

            images:
            [
            {
                label: "Flights within the US",
                svgPath: planeSVG,
                left: 90,
                top: 20,
                labelShiftY: 5,
                color: "#CC0000",
                labelColor: "#CC0000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 20
            },
            {
                label: "Show flights from Paris",
                left: 96,
                top: 45,
                labelColor: "#000000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 11,
                linkToObject: "Paris"
            },
            {
                label: "Show all flights",
                left: 96,
                top: 60,
                labelColor: "#000000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 11,
                linkToObject: "All"
            }]
        };

        //All flights
        var all =
        {
            id: "All",
            color: "#000000",
            title: "All Flights",
            latitude: 0,
            longitude: 0,
            scale: 1.5,
            zoomLevel: 2.4,
            zoomLongitude: -59,
            zoomLatitude: 30,

            lines:
            [
                {
                    //Paris - New York
                    latitudes: [48.8742, 40.4300],
                    longitudes: [2.3470, -74.0000]
                },
                {
                    //Paris - Atlanta
                    latitudes: [48.8742, 33.7489],
                    longitudes: [2.3470, -84.3881]
                },
                {
                    //Paris - FL
                    latitudes: [48.8742, 26.1219],
                    longitudes: [2.3470, -80.1436]
                },
                {
                    //Paris - Pau
                    latitudes: [48.8742, 43.3017],
                    longitudes: [2.3470, -0.1]
                },
                {
                    //Paris - Chicago
                    latitudes: [48.8742, 41.850],
                    longitudes: [2.3470, -87.6500]
                },
                {
                    //Paris - Douala
                    latitudes: [48.8742, 4.0500],
                    longitudes: [2.3470, 9.7000]
                },
                {
                    //Paris - Bordeaux
                    latitudes: [48.8742, 44.8386],
                    longitudes: [2.3470, -0.5783]
                },
                {
                    //Nantes - Venice
                    latitudes: [47.2181, 45.4333],
                    longitudes: [-1.5528, 12.3167]
                },
                {
                    //Brest - Lyon
                    latitudes: [48.3908, 45.7669],
                    longitudes: [-4.4856, 4.8342]
                },
                {
                    //Lyon - Toulouse
                    latitudes: [45.7669, 43.6053],
                    longitudes: [4.8342, 1.4428]
                    },
                {   //FL - Chicago
                    latitudes: [26.1219, 41.8500],
                    longitudes: [-80.1436, -87.8500]
                },
                {   //FL - San Francisco
                    latitudes: [26.1219, 37.7750],
                    longitudes: [-80.1436, -122.4183]
                },
                {
                    //FL - Atlanta
                    latitudes: [26.1219, 33.7489],
                    longitudes: [-80.1436, -84.3881]
                },
                {
                    //FL - Phoenix
                    latitudes: [26.1219, 33.4482],
                    longitudes: [-80.1436, -112.0738]
                },
                {
                    //FL - Las Vegas
                    latitudes: [26.1219, 36.0800],
                    longitudes: [-80.1436, -115.1522]
                },
                {
                    //Las Vegas - Dallas
                    latitudes: [36.0800, 32.7828],
                    longitudes: [-115.1522, -96.8039]
                },
                {
                    //Dallas - FL
                    latitudes: [32.7828, 26.1219],
                    longitudes: [-96.8039, -80.1436]
                },
                {
                    //FL - New York
                    latitudes: [26.1219, 40.43],
                    longitudes: [-80.1436, -74]
                },
                {
                    //FL - Washington
                    latitudes: [26.1219, 38.8900],
                    longitudes: [-80.1436, -77.0300]
                },
                {
                    //Washington - New York
                    latitudes: [38.8900, 40.43],
                    longitudes: [-77.0300, -74]
                },
                {
                    //Chicago - Seattle
                    latitudes: [41.8500, 47.6097],
                    longitudes: [-87.6500, -122.3331]
                },
                {
                    //New York - Chicago
                    latitudes: [40.43, 41.8500],
                    longitudes: [-74, -87.6500]
                },
                {
                    //Las Vegas - Cincinnati
                    latitudes: [36.0800, 39.1619],
                    longitudes: [-115.1522, -84.4569]
                },
                {
                    //Cincinnati - FL
                    latitudes: [39.1619, 26.1219],
                    longitudes: [-84.4569, -80.1436]
                }
            ],

            images:
            [
            {
                label: "All Flights",
                svgPath: planeSVG,
                left: 90,
                top: 20,
                labelShiftY: 5,
                color: "#CC0000",
                labelColor: "#CC0000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 20
            },
            {
                label: "Show flights from Paris",
                left: 96,
                top: 45,
                labelColor: "#000000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 11,
                linkToObject: "Paris"
            },
            {
                label: "Show flights within the US",
                left: 96,
                top: 60,
                labelColor: "#000000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 11,
                linkToObject: "US"
            }]
        };

        //Cities
        var cities =
        [
            paris,
            US,
            all,
            {
                svgPath: targetSVG,
                title: "New York",
                latitude: 40.43,
                longitude: -74
            },
            {
                svgPath: targetSVG,
                title: "Atlanta",
                latitude: 33.7489,
                longitude: -84.3881
            },
            {
                svgPath: targetSVG,
                title: "Chicago",
                latitude: 41.8500,
                longitude: -87.8500
            },
            {
                svgPath: targetSVG,
                title: "Pau",
                latitude: 43.2017,
                longitude: -0.1
            },
            {
                svgPath: targetSVG,
                title: "Douala",
                latitude: 4.0500,
                longitude: 9.7000
            },
            {
                svgPath: targetSVG,
                title: "San Francisco",
                latitude: 37.7750,
                longitude: -122.4183
            },
            {
                svgPath: targetSVG,
                title: "Phoenix",
                latitude: 33.4482,
                longitude: -112.0738
            },
            {
                svgPath: targetSVG,
                title: "Las Vegas",
                latitude: 36.0800,
                longitude: -115.1522
            },
            {
                svgPath: targetSVG,
                title: "Dallas",
                latitude: 32.7828,
                longitude: -96.8039
            },
            {
                svgPath: targetSVG,
                title: "Washington",
                latitude: 38.8900,
                longitude: -77.0300
            },
            {
                svgPath: targetSVG,
                title: "Seattle",
                latitude: 47.6097,
                longitude: -122.3331
            },
            {
                svgPath: targetSVG,
                title: "Cincinnati",
                latitude: 39.1619,
                longitude: -84.4569
            },
            {
                svgPath: targetSVG,
                title: "Denver",
                latitude: 39.7392,
                longitude: -104.9842
            },
            {
                svgPath: targetSVG,
                title: "Nantes",
                latitude: 47.2181,
                longitude: -1.5528
            },
            {
                svgPath: targetSVG,
                title: "Brest",
                latitude: 48.3908,
                longitude: -4.4856
            },
            {
                svgPath: targetSVG,
                title: "Lyon",
                latitude: 45.7669,
                longitude: 4.8342
            },
            {
                svgPath: targetSVG,
                title: "Toulouse",
                latitude: 43.4053,
                longitude: 1.4428
            },
            {
                svgPath: targetSVG,
                title: "Venice",
                latitude: 45.4333,
                longitude: 12.3167
            },
            {
                svgPath: targetSVG,
                title: "Bordeaux",
                latitude: 44.8386,
                longitude: -0.5783
            }
        ];

          
        dataProvider.linkToObject = all;
        dataProvider.images = cities;
        map.dataProvider = dataProvider;
        map.mouseWheelZoomEnabled = true;
        map.zoomControl.buttonSize = 12;
        map.zoomControl.panControlEnabled = false;
        map.zoomControl.top = 100;
        map.zoomControl.zoomControlEnabled = false;
        map.linesSettings.color = "#333333";

        map.write("FlightsMap");

    });
}