﻿function sendMail()
{
    var json = { name: $("#name").val(), email: $("#email").val(), phone: $("#phone").val(), message: $("#message").val() }

    $.ajax(
	{
	    type: "POST",
	    url: '/Home/SendMail',
	    dataType: 'json',
	    data: json,
	    cache: false,
	    success: function (response)
	    {
	        if (response.success)
	        {
	            $("#modalBody").html("<div class='alert alert-success'>Your message was successfully sent. I will get back to you as soon as I can.</div>");
	            $("#modalButtonSend").remove();
	            $("#modalButtonClose").attr("class", "btn span4 offset4");
	        }
	        else
	        {
	            $("#alert").html("<div class='alert alert-danger'>" + response.msg + "</div>");
	        }	        
	    }
	});
}

function toggle(id, accordion)
{
    if ($(id).hasClass("icon-double-angle-right"))
    {
        $(accordion + " .icon-double-angle-down").removeClass("icon-double-angle-down").addClass("icon-double-angle-right");
        $(id).removeClass("icon-double-angle-right").addClass("icon-double-angle-down");
    }
    else
    {
        $(accordion + " .icon-double-angle-down").removeClass("icon-double-angle-down").addClass("icon-double-angle-right");
        $(id).removeClass("icon-double-angle-down").addClass("icon-double-angle-right");
    }

    setTimeout(function () { resize(); }, 200);
}