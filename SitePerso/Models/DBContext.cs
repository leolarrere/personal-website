﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace SitePerso.Models
{
    public class DB : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Project> Projects { get; set; }

        public DB() :base("name=DBConnectionString") {}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>().ToTable("Posts");
            modelBuilder.Entity<Post>().HasKey(p => p.id);
            modelBuilder.Entity<Post>().Property(p => p.id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Post>().Property(p => p.content).HasMaxLength(1000);
            modelBuilder.Entity<Post>().Property(p => p.title).HasMaxLength(100);
            modelBuilder.Entity<Post>().Property(p => p.fileName).HasMaxLength(200);

            modelBuilder.Entity<Comment>().ToTable("Comments");
            modelBuilder.Entity<Comment>().HasKey(c => c.id);
            modelBuilder.Entity<Comment>().Property(c => c.id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Comment>().Property(c => c.content).HasMaxLength(1000);
            modelBuilder.Entity<Comment>().Property(c => c.username).HasMaxLength(50);
            //Foreign key + cascade on delete
            modelBuilder.Entity<Comment>().HasRequired(c => c.post).WithMany(p => p.Comments).HasForeignKey(c => c.idPost).WillCascadeOnDelete(true);

            modelBuilder.Entity<Experience>().ToTable("Experiences");
            modelBuilder.Entity<Experience>().HasKey(e => e.id);
            modelBuilder.Entity<Experience>().Property(e => e.id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Task>().ToTable("Tasks");
            modelBuilder.Entity<Task>().HasKey(t => t.id);
            modelBuilder.Entity<Task>().Property(t => t.id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            //Foreign key + cascade on delete
            modelBuilder.Entity<Task>().HasRequired(t => t.experience).WithMany(e => e.Tasks).HasForeignKey(t => t.idExperience).WillCascadeOnDelete(true);

            base.OnModelCreating(modelBuilder);
        }
    }
}