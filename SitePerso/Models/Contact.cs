﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SitePerso.Models
{
    public class Contact
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }        
        public string phone { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(125)]   
        public string message { get; set; }
    }
}