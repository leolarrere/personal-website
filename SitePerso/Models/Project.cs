﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitePerso.Models
{
    public class Project
    {
        public int id { get; set; }
        public string name { get; set; }
        public string date { get; set; }
        public string imgURL { get; set; }
        public string institution { get; set; }
        public string shortDescription { get; set; }
        public string description { get; set; }
        public string technology { get; set; } 
        public int? order { get; set; }
    }
}