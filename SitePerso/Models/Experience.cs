﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitePerso.Models
{
    public class Experience
    {
        public int id { get; set; }
        public string startMonth { get; set; }
        public string endMonth { get; set; }
        public int? startYear { get; set; }
        public int? endYear { get; set; }
        public string imgURL { get; set; }
        public string companyName { get; set; }
        public string jobDescription { get; set; }
        public int? order { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
    }
}