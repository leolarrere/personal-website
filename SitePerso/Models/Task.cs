﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitePerso.Models
{
    public class Task
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public virtual int? idExperience { get; set; }
        public virtual Experience experience { get; set; }
    }
}