﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitePerso.Models
{
    public class Post
    {
        public int id { get; set; }     
        public string title { get; set; }
        public string content { get; set; }
        public DateTime date { get; set; }
        public string fileName { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}