﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitePerso.Models
{
    public class Comment
    {
        public int id { get; set; }
        public string username { get; set; }
        public string content { get; set; }
        public DateTime date { get; set; }
        public Boolean approved { get; set; }  
        public virtual int? idPost { get; set; }
        public virtual Post post { get; set; }
    }
}